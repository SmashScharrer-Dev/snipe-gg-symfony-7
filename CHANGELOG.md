# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

---

## [0.11.1] - 2025-02-03

⚙️Fix : KDA bug "Division by 0"

### Fixed

- Fix a bug in KDA card-content, because there was a division by 0 process with these datas : nbrKills and nbrDeaths ;

---

## [0.11.0] - 2025-01-16

📦 New : Summoner spells data

### Added

- New API : DDragonSummonerSpellAPI ;
- New DTO : DDragonSummonerSpellDTO ;
- New Service : DDragonSummonerSpellService ;
- Download & use new JavaScript library : Tippy ;

### Changed

- Update HomeController : Setup new requests to take summoner spells datas from each player ;
- Update Home template : Show summoner spells informations, with Tippy tooltip library ;
- Rename some DDragon classes, and methods, for a better understanding ;

---

## [0.10.0] - 2025-01-12

📦 New : Player latest datas match/game datas

### Added

- New DTO : AbstractMatchDTO and MatchSoloQueueDTO (extends AbstractMatchDTO class) ;
- New API : MatchRiotAPI ;
- New Service : MatchService ;
- New style partial : Add card to show last match datas ;

### Changed

- Website falvicon ;
- Update NPM & Composer dependencies ;
- Update Symfony config to add new API service ;
- Update HomeController : Add requests to have some summoners last match datas ;
- Update home Twig template : Add card to show last summoner match data ;

### Fixed

- Fix PHPStan & PHP-CS-Fixer errors ;

---

## [0.9.0] - 2024-07-11

📦 New : League datas

### Added

- New API : LeagueRiotAPI ;
- New DTO : LeagueSoloQueueDTO (extends AbstractLeagueQueueDTO class) ;
- New Service : LeagueService ;
- New config file for PHPInsights library ;

### Changed

- Update SummonerDTO : Add new property => LeagueSoloQueueDTO ;
- Update SCSS assets : Add card border color for league summoner tier ;
- Update Symfony config to add new League API service ;
- Update HomeController : Add League service request ;
- Update home Twig template : Add tier & rank datas for each summoner ;

### Fixed

- Fix PHPStan & PHP-CS-Fixer errors ;

---

## [0.8.0] - 2024-07-10

📦 New : Ddragon versions API requests

### Added

- New API : DragonVersionsAPI => To request all & latest League of Legends versions ;
- New DTO : DragonDTO => To store datas from DDragon API ;
- New Service : DragonVersionsService => To do DDragon API requests & store datas into DragonDTO object ;

### Changed

- Update HomeController : Add DDragon latest version ;
- Update home template : Show summoner profile icon ;

---

## [0.7.0] - 2024-07-10

📦 New : Bootswatch

### Added

- New style library : Booswatch (Boostrap theme) ;

### Changed

- Update global SCSS file ;

---

## [0.6.0] - 2024-07-10

📦 New : Cache Service

### Added

- New Service :  CacheService PHP class => To store data from Riot API ;

### Changed

- Update Symfony cache config file ;
- Update HomeController to add CacheService service into Riot API requests ;

---

## [0.5.0] - 2024-07-09

📦 New : Home Page

### Added

- New project home page ;
- New SCSS file for home page ;
- Add Bootstrap & Swiper JS librairies ;

### Changed

- Update some scripts of GitLab CI ;

### Fixed

- Fix PHPStan & PHP-CS-Fixer errors ;

---

## [0.4.0] - 2024-07-08

🛠️ Setup : WebpackEncore

### Added

- Add WebbackEcore Symfony library ;
- Install & Setup Bootstrap style library in project ;

---

## [0.3.0] - 2024-07-08

📦 New : Account & Summoner Riot API requests

### Added

- New DTO : Summoner (for all player/summoner data) ;
- New API requests Riot<=>App : Account & Summoner datas ;
- New Services : Riot API requests <=> DTO ;
- New Exception classes : Show customized errors when Riot API requests don't work ;

### Changed

- Composer : Updat scripts ;

---

## [0.2.0] - 2024-07-06

🛠️ Init & Setup : Gitlab CI

### Added

- Init & setup Gitlab CI with PHP tools ;

---

## [0.1.0] - 2024-07-06

🛠️ Init & Setup : Application project

### Added

- Creat Symfony project & generate all files ;
- Init .editorconfig / .gitignore / CHANGELOG.md / README.md files ;
- Init & Setup PHP-CS-Fixer & PHPStan & PHPInsights libraries ;
