<?php

declare(strict_types=1);

namespace App\Controller;

use App\API\DDragon\DDragonSummonerSpellAPI;
use App\Services\CacheService;
use App\Services\DDragon\DDragonGameVersionService;
use App\Services\DDragon\DDragonSummonerSpellService;
use App\Services\Riot\LeagueService;
use App\Services\Riot\MatchService;
use App\Services\Riot\SummonerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(name: 'app.home.')]
final class HomeController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function __invoke(
        SummonerService $summonerService,
        DDragonGameVersionService $dragonGameVersionService,
        DDragonSummonerSpellService $dragonSummonerSpellService,
        LeagueService $leagueService,
        MatchService $matchService,
        CacheService $cacheService,
        DDragonSummonerSpellAPI $ddragonSummonerSpellAPI,
    ): Response {
        // On initialise les variables.
        $summoners = [];
        $listSummoners = [
            ['Demyqewiki2', 'EUW'],
            ['SmashSLeRetour', 'EUW'],
            ['SmashScharrer2', 'EUW'],
            ['Sn0W38', '3033'],
            //['LeRetourDuNezGro', '21450'],
            //['BouffeTaReum', '7592'],
            //['BouffeMonSarce', '9733'],
        ];
        $lastestMatches = [];

        // Pour chaque joueur (ou "summoner") : on récupère les informations du joueur, son rang en soloQ, et sa dernière partie.
        foreach ($listSummoners as $summoner) {
            $summonerData = $cacheService->getCachedData('summoner_'.$summoner[0], function () use ($summoner, $summonerService) {
                return $summonerService->getSummoner($summoner[0], $summoner[1]);
            });

            $leagueSoloQueueData = $cacheService->getCachedData('summoner_'.$summoner[0].'_solo_queue', function () use ($summonerData, $leagueService) {
                return $leagueService->getSoloQueueLeague($summonerData);
            });

            $summonerData->setLeagueSoloQueue($leagueSoloQueueData);

            $summoners[] = $summonerData;
        }

        // On récupère la dernière version du jeu pour pouvoir afficher les images des sorts d'invocateur.
        $ddragonLatestGameVersionData = $cacheService->getCachedData('dragon_latest_version', function () use ($dragonGameVersionService) {
            return $dragonGameVersionService->getDDragonLatestGameVersion();
        });

        $summonerSpellsData = $ddragonSummonerSpellAPI->findAllSummonerSpells($ddragonLatestGameVersionData->getLatestVersion());

        // Pour chaque joueur (ou "summoner") : on récupère sa dernière partie et on remplace les ID des sorts d'invocateur par les données des sorts d'invocateur.
        foreach ($summoners as $summoner) {
            $latestMatch = $cacheService->getCachedData('summoner_'.$summoner->getGameName().'_latest_match', function () use ($summoner, $matchService) {
                return $matchService->getMatch($summoner);
            });

            $listSummonerSpells = [];

            foreach ($latestMatch->getSummonerSpells() as $summonerSpell) {
                $summonerSpellData = $dragonSummonerSpellService->getDDragonSummonerSpellsByID($summonerSpellsData, $summonerSpell);

                $listSummonerSpells[] = $summonerSpellData;
            }

            $latestMatch->setSummonerSpells($listSummonerSpells);

            $lastestMatches[$summoner->getGameName()] = $latestMatch;
        }

        return $this->render('home/index.html.twig', [
            'summoners' => $summoners,
            'latestGameVersion' => $ddragonLatestGameVersionData->getLatestVersion(),
            'latestMatches' => $lastestMatches,
        ]);
    }
}
