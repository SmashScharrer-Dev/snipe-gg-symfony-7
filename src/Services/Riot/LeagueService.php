<?php

declare(strict_types=1);

namespace App\Services\Riot;

use App\API\Riot\League\LeagueRiotAPI;
use App\DTO\League\LeagueSoloQueueDTO;
use App\DTO\SummonerDTO;

/**
 * Class LeagueService : Service permettant de récupérer les données provenant de l'API Riot Games, pour créer des objects "LeagueSoloQueueDTO".
 */
final class LeagueService
{
    public function __construct(
        private readonly LeagueRiotAPI $leagueRiotAPI,
    ) {
    }

    /**
     * Récupérer les données provenant de l'API Riot "League" pour les stocker dans l'objet "LeagueSoloQueueDTO".
     */
    public function getSoloQueueLeague(SummonerDTO $summonerDTO): LeagueSoloQueueDTO
    {
        $leagueRequest = $this->leagueRiotAPI->findLeagueQueueLeagueBySummonerID($summonerDTO->getSummonerId());

        if (empty($leagueRequest)) {
            return new LeagueSoloQueueDTO(
                'UNRANKED',
                'SOLO',
                'UNRANKED',
                'UNRANKED',
                0,
                0,
                0
            );
        }

        $filteredLeagues = array_filter($leagueRequest, function ($league) {
            return 'RANKED_SOLO_5x5' === $league['queueType'];
        });

        $filteredLeagues = array_values($filteredLeagues);

        $response = $filteredLeagues[0];

        if (!is_array($response) || !isset($response['leagueId'], $response['tier'], $response['rank'], $response['leaguePoints'], $response['wins'], $response['losses'])) {
            return new LeagueSoloQueueDTO(
                'UNRANKED',
                'SOLO',
                'UNRANKED',
                'UNRANKED',
                0,
                0,
                0
            );
        }

        return new LeagueSoloQueueDTO(
            (string) $response['leagueId'],
            'SOLO',
            (string) $response['tier'],
            (string) $response['rank'],
            (int) $response['leaguePoints'],
            (int) $response['wins'],
            (int) $response['losses']
        );
    }
}
