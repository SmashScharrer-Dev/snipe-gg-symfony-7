<?php

declare(strict_types=1);

namespace App\Services\Riot;

use App\API\Riot\Match\MatchRiotAPI;
use App\DTO\Match\MatchSoloQueueDTO;
use App\DTO\SummonerDTO;
use App\Services\FilteredArrayService;

/**
 * Summary of MatchService.
 */
final class MatchService
{
    public function __construct(
        private readonly MatchRiotAPI $matchRiotAPI,
        private FilteredArrayService $filteredArrayService,
    ) {
    }

    /**
     * Récupérer les données provenant de l'API Riot "Match" pour les stocker dans l'objet "MatchSoloQueuDTO".
     */
    public function getMatch(SummonerDTO $summonerDTO): MatchSoloQueueDTO
    {
        $idMatchRequest = $this->matchRiotAPI->findOneMatchBySummonerPUUID($summonerDTO);

        $matchDataRequest = $this->matchRiotAPI->findMatchDataByMatchID((string) $idMatchRequest);

        // @phpstan-ignore-next-line
        $matchDataFiltered = $this->filteredArrayService->filteredArrayByKey((array) $matchDataRequest['info']['participants'], 'puuid', $summonerDTO->getPuuid());

        return new MatchSoloQueueDTO(
            // @phpstan-ignore-next-line
            (string) $matchDataRequest['metadata']['matchId'],
            // @phpstan-ignore-next-line
            (int) $matchDataRequest['info']['gameDuration'],
            (string) $matchDataFiltered['role'],
            (int) $matchDataFiltered['championId'],
            (string) $matchDataFiltered['championName'],
            (int) $matchDataFiltered['goldEarned'],
            (int) $matchDataFiltered['kills'],
            (int) $matchDataFiltered['assists'],
            (int) $matchDataFiltered['deaths'],
            [
                (int) $matchDataFiltered['item0'],
                (int) $matchDataFiltered['item1'],
                (int) $matchDataFiltered['item2'],
                (int) $matchDataFiltered['item3'],
                (int) $matchDataFiltered['item4'],
                (int) $matchDataFiltered['item5'],
                (int) $matchDataFiltered['item6'],
            ],
            [
                'primary' => [
                    (int) $matchDataFiltered['perks']['styles'][0]['selections'][0]['perk'],
                    (int) $matchDataFiltered['perks']['styles'][0]['selections'][1]['perk'],
                    (int) $matchDataFiltered['perks']['styles'][0]['selections'][2]['perk'],
                    (int) $matchDataFiltered['perks']['styles'][0]['selections'][3]['perk'],
                ],
                'secondary' => [
                    (int) $matchDataFiltered['perks']['styles'][1]['selections'][0]['perk'],
                    (int) $matchDataFiltered['perks']['styles'][1]['selections'][1]['perk'],
                ],
            ],
            [
                (int) $matchDataFiltered['summoner1Id'],
                (int) $matchDataFiltered['summoner2Id'],
            ],
            (bool) $matchDataFiltered['win']
        );
    }
}
