<?php

declare(strict_types=1);

namespace App\Services\Riot;

use App\API\Riot\Account\AccountRiotAPI;
use App\API\Riot\Summoner\SummonerRiotAPI;
use App\DTO\SummonerDTO as Summoner;
use App\Exception\API\Riot\AccountRiotAPIException;
use App\Exception\API\Riot\SummonerRiotAPIException;

/**
 * Class SummonerService : Service permettant de récupérer les données provenant de l'API Riot Games, pour créer des objects "SummonerDTO".
 */
final class SummonerService
{
    public function __construct(
        private readonly AccountRiotAPI $accountAPI,
        private readonly SummonerRiotAPI $summonerAPI,
    ) {
    }

    /**
     * Récupérer les données provenant de l'API Riot "Summoner" pour les stocker dans l'objet "SummonerDTO".
     *
     * @throws AccountRiotAPIException
     * @throws SummonerRiotAPIException
     */
    public function getSummoner(
        string $summonerGameName,
        string $summonerTagLine,
    ): Summoner {
        $accountRequest = $this->accountAPI->findAccountByGameNameAndTagLine($summonerGameName, $summonerTagLine);

        if (!isset($accountRequest['puuid'])) {
            throw new AccountRiotAPIException($summonerGameName);
        }

        $summonerRequest = $this->summonerAPI->findSummonerByPUUID($accountRequest['puuid']);

        if (!isset($summonerRequest['puuid'])) {
            throw new SummonerRiotAPIException($summonerGameName);
        }

        return new Summoner(
            $summonerRequest['id'],
            $summonerRequest['accountId'],
            $summonerRequest['puuid'],
            $summonerTagLine,
            $summonerGameName,
            (int) $summonerRequest['profileIconId'],
            (int) $summonerRequest['summonerLevel'],
            null
        );
    }
}
