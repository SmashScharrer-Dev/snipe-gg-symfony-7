<?php

declare(strict_types=1);

namespace App\Services\DDragon;

use App\API\DDragon\DDragonGameVersionAPI;
use App\DTO\DDragon\DDragonGameVersionDTO;
use App\Exception\API\DDragon\DDragonAPIRequestException;

/**
 * Class DDragonGameVersionService : Service permettant de récupérer les versions du jeu provenant de l'API "DDragon",
 * pour créer un objet DTO "DragonGameVersionDTO".
 */
final class DDragonGameVersionService
{
    public function __construct(
        private DDragonGameVersionAPI $dragonGameVersionAPI,
    ) {
    }

    /**
     * Récupérer la dernière version du jeu "League of Legends", sur l'API "DDragon",
     * pour le stocker dans l'objet "DragonGameVersionDTO".
     *
     * @throws DDragonAPIRequestException
     */
    public function getDDragonLatestGameVersion(): DDragonGameVersionDTO
    {
        $dragonLatestVersionRequest = $this->dragonGameVersionAPI->findLatestGameVersion();

        if (empty($dragonLatestVersionRequest)) {
            throw new DDragonAPIRequestException('Aucune version n\'a été trouvée.', 500, new \Exception());
        }

        return new DDragonGameVersionDTO(
            $dragonLatestVersionRequest
        );
    }
}
