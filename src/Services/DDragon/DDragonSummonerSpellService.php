<?php

declare(strict_types=1);

namespace App\Services\DDragon;

use App\DTO\DDragon\DDragonSummonerSpellDTO;
use App\Exception\API\DDragon\DDragonAPIRequestException;

/**
 * Class DDragonSummonerSpellService : Service permettant de récupérer les données liées aux sorts d'invocateur (ou "summoner spell"),
 * provenant de l'API "DDragon", pour créer un objet DTO "DDragonSummonerSpellDTO".
 */
final class DDragonSummonerSpellService
{
    /**
     * Récupérer les données liées à un sort d'invocateur (ou "summoner spell") sur l'API "DDragon",
     * à partir de la donnée "ID" du sort d'invocateur récupérable avec l'API "Riot",
     * pour les stocker dans l'objet "DDragonSummonerSpellDTO".
     *
     * @param array<mixed> $dataDDragonSummonerSpells
     */
    public function getDDragonSummonerSpellsByID(array $dataDDragonSummonerSpells, int $id): DDragonSummonerSpellDTO
    {
        foreach ($dataDDragonSummonerSpells['data'] as $summonerSpell) {
            if ($summonerSpell['key'] === (string) $id) {
                return new DDragonSummonerSpellDTO(
                    (int) $summonerSpell['key'],
                    $summonerSpell['name'],
                    $summonerSpell['description']
                );
            }
        }

        throw new DDragonAPIRequestException('Aucun sort d\'invocateur n\'a été trouvé.', 500, new \Exception());
    }
}
