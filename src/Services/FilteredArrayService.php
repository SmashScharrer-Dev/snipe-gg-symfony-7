<?php

declare(strict_types=1);

namespace App\Services;

/**
 * Summary of FilteredArrayService.
 */
final class FilteredArrayService
{
    /**
     * Summary of filteredArrayByKey.
     *
     * @param array<mixed> $arrayData
     *
     * @return array<mixed>|null
     */
    public function filteredArrayByKey(array $arrayData, string $arrayKey, string $data): ?array
    {
        foreach ($arrayData as $array) {
            if ($data === $array[$arrayKey]) {
                return $array;
            }
        }

        return null;
    }
}
