<?php

declare(strict_types=1);

namespace App\Services;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class CacheService : Mettre en cache les données des diverses requêtes.
 */
final class CacheService
{
    public function __construct(private readonly CacheInterface $cacheInterface)
    {
    }

    /**
     * Mettre en cache les données en provenance d'une requête ou autre.
     */
    public function getCachedData(string $key, callable $callback, int $expiresAfter = 3600): object
    {
        return $this->cacheInterface->get($key, function (ItemInterface $item) use ($callback, $expiresAfter) {
            $item->expiresAfter($expiresAfter);

            return $callback();
        });
    }
}
