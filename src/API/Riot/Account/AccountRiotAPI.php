<?php

declare(strict_types=1);

namespace App\API\Riot\Account;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class AccountRiotAPI : Lister les différentes requêtes HTTP pour récupérer les donnéees de l'API Riot Games "Account".
 */
final class AccountRiotAPI
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiLink,
        private readonly string $apiKey,
    ) {
    }

    /**
     * Rechercher les informations du compte d'un joueur (id, puuid, etc...) sur l'API de Riot Games
     * en fonction de son pseudo et de sa région/tagline (#EUW, etc...).
     *
     * @return array<string>
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function findAccountByGameNameAndTagLine(string $gameName, string $tagLine): array
    {
        $request = $this->client->request(
            'GET',
            $this->apiLink.'/riot/account/v1/accounts/by-riot-id/'.$gameName.'/'.$tagLine,
            [
                'query' => [
                    'api_key' => $this->apiKey,
                ],
            ]
        );

        return $request->toArray();
    }
}
