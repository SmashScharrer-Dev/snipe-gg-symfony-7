<?php

declare(strict_types=1);

namespace App\API\Riot\League;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class LeagueRiotAPI : Lister les différentes requêtes HTTP pour récupérer les donnéees de l'API Riot Games "League".
 */
final class LeagueRiotAPI
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiLink,
        private readonly string $apiKey,
    ) {
    }

    /**
     * Rechercher les stats "SoloQueue" d'un joueur (pseudo, level, etc...) sur l'API de Riot Games
     * en fonction d'une donnée de son compte : "AccountID".
     *
     * @return array<int, array<string, mixed>>
     */
    public function findLeagueQueueLeagueBySummonerID(string $summonerID): array
    {
        $request = $this->client->request(
            'GET',
            $this->apiLink.'/lol/league/v4/entries/by-summoner/'.$summonerID,
            [
                'query' => [
                    'api_key' => $this->apiKey,
                ],
            ]
        );

        return $request->toArray();
    }
}
