<?php

declare(strict_types=1);

namespace App\API\Riot\Match;

use App\DTO\SummonerDTO;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class MatchRiotAPI : Lister les différentes requêtes HTTP pour récupérer les donnéees de l'API Riot Games "Match".
 */
final class MatchRiotAPI
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiLink,
        private readonly string $apiKey,
    ) {
    }

    /**
     * Récupérer les dernières games d'un joueur, en fonction de son "PUUID".
     */
    public function findOneMatchBySummonerPUUID(SummonerDTO $summonerDTO): string
    {
        $request = $this->client->request(
            'GET',
            $this->apiLink.'/lol/match/v5/matches/by-puuid/'.$summonerDTO->getPuuid().'/ids',
            [
                'query' => [
                    'api_key' => $this->apiKey,
                    'count' => 1,
                ],
            ]
        );

        $response = $request->toArray();

        return (string) $response[0];
    }

    /**
     * Récupérer une game à partir de la donnée "ID" de la game.
     *
     * @return array<string|int|bool>
     */
    public function findMatchDataByMatchID(string $idMatch): array
    {
        $request = $this->client->request(
            'GET',
            $this->apiLink.'/lol/match/v5/matches/'.$idMatch,
            [
                'query' => [
                    'api_key' => $this->apiKey,
                ],
            ]
        );

        return $request->toArray();
    }
}
