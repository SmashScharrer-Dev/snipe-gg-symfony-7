<?php

declare(strict_types=1);

namespace App\API\Riot\Summoner;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class SummonerRiotAPI : Lister les différentes requêtes HTTP pour récupérer les donnéees de l'API Riot Games "Summoner".
 */
final class SummonerRiotAPI
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiLink,
        private readonly string $apiKey,
    ) {
    }

    /**
     * Rechercher les informations d'un joueur (pseudo, level, etc...) sur l'API de Riot Games
     * en fonction d'une donnée de son compte : "PUUID".
     *
     * @return array<string|int>
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function findSummonerByPUUID(string $summonerPUUID): array
    {
        $request = $this->client->request(
            'GET',
            $this->apiLink.'/lol/summoner/v4/summoners/by-puuid/'.$summonerPUUID,
            [
                'query' => [
                    'api_key' => $this->apiKey,
                ],
            ]
        );

        return $request->toArray();
    }
}
