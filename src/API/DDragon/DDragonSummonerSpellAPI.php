<?php

declare(strict_types=1);

namespace App\API\DDragon;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class DDragonSummonerSpellAPI : Lister les différents spells d'invocateurs, en fonction de la version du jeu (anciennes et récentes).
 */
final class DDragonSummonerSpellAPI
{
    public function __construct(
        private HttpClientInterface $httpClientInterface,
        private readonly string $apiLink,
    ) {
    }

    /**
     * Lister toutes les informations concernant les "summoner spells" (spells d'invocateurs) en fonction de la version du jeu.
     *
     * @return array<string, mixed>
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function findAllSummonerSpells(string $version): array
    {
        $request = $this->httpClientInterface->request(
            'GET',
            $this->apiLink.'/cdn/'.$version.'/data/fr_FR/summoner.json',
            []
        );

        return $request->toArray();
    }
}
