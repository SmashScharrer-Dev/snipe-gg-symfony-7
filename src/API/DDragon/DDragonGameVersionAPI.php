<?php

declare(strict_types=1);

namespace App\API\DDragon;

use App\Exception\API\DDragon\DDragonAPIRequestException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class DDragonGameVersionAPI : Lister les différentes versions du jeu (anciennes et récentes).
 */
final class DDragonGameVersionAPI
{
    public function __construct(
        private readonly HttpClientInterface $httpClientInterface,
        private readonly string $apiLink,
    ) {
    }

    /**
     * Récupérer toutes les versions du jeu "League of Legends", via l'API "DDragon".
     *
     * @return array<int, string>
     *
     * @throws DDragonAPIRequestException
     */
    public function findAllGameVersions(): array
    {
        try {
            $request = $this->httpClientInterface->request(
                'GET',
                $this->apiLink.'/api/versions.json',
                []
            );

            return $request->toArray();
        } catch (\Exception $exception) {
            throw new DDragonAPIRequestException('Erreur dans la récupération de la version du jeu'.$exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * Récupérer la dernière version du jeu "League of Legends", via l'API "DDragon".
     */
    public function findLatestGameVersion(): string
    {
        $request = $this->findAllGameVersions();

        return $request[0];
    }
}
