<?php

declare(strict_types=1);

namespace App\Exception\API\Riot;

/**
 * Class Exception SummonerRiotAPIException : Renvoyer un message d'erreur personnalisée si une requête API ne fonctionne pas.
 */
final class SummonerRiotAPIException extends \Exception
{
    public function __construct(string $summonerGameName)
    {
        $message = sprintf('Aucun invocateur trouvé avec le nom "%s".', $summonerGameName);
        parent::__construct($message, 500);
    }
}
