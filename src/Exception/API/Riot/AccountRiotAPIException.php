<?php

declare(strict_types=1);

namespace App\Exception\API\Riot;

/**
 * Class Exception AccountRiotAPIException : Renvoyer un message d'erreur personnalisée si une requête API ne fonctionne pas.
 */
final class AccountRiotAPIException extends \Exception
{
    public function __construct(string $summonerGameName)
    {
        $message = sprintf('Aucun compte trouvé avec le pseudo "%s".', $summonerGameName);
        parent::__construct($message, 500);
    }
}
