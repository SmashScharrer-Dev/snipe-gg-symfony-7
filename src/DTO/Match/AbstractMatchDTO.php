<?php

declare(strict_types=1);

namespace App\DTO\Match;

abstract class AbstractMatchDTO
{
    private readonly string $matchId;
    private readonly int $duration;
    private readonly string $role;
    private readonly int $championId;
    private readonly string $championName;
    private readonly int $goldEarned;
    private readonly int $nbrKills;
    private readonly int $nbrAssists;
    private readonly int $nbrDeaths;
    /** @var array<int> */
    private readonly array $items;
    /** @var array<mixed> */
    private readonly array $runes;
    /** @var array<mixed> */
    private array $summonerSpells;
    private readonly bool $isWin;

    /**
     * @param array<mixed> $itemsParam
     * @param array<mixed> $runesParam
     * @param array<mixed> $summonerSpellsParam
     */
    public function __construct(
        string $matchIdParam,
        int $durationParam,
        string $roleParam,
        int $championIdParam,
        string $championNameParam,
        int $goldEarnedParam,
        int $nbrKillsParam,
        int $nbrAssistsParam,
        int $nbrDeathsParam,
        array $itemsParam,
        array $runesParam,
        array $summonerSpellsParam,
        bool $isWinParam,
    ) {
        $this->matchId = $matchIdParam;
        $this->duration = $durationParam;
        $this->role = $roleParam;
        $this->championId = $championIdParam;
        $this->championName = $championNameParam;
        $this->goldEarned = $goldEarnedParam;
        $this->nbrKills = $nbrKillsParam;
        $this->nbrAssists = $nbrAssistsParam;
        $this->nbrDeaths = $nbrDeathsParam;
        $this->items = $itemsParam;
        $this->runes = $runesParam;
        $this->summonerSpells = $summonerSpellsParam;
        $this->isWin = $isWinParam;
    }

    public function getMatchId(): string
    {
        return $this->matchId;
    }

    /**
     * Game duration in seconds.
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getChampionID(): int
    {
        return $this->championId;
    }

    public function getChampionName(): string
    {
        return $this->championName;
    }

    public function getGoldEarned(): int
    {
        return $this->goldEarned;
    }

    public function getNbrKills(): int
    {
        return $this->nbrKills;
    }

    public function getNbrAssists(): int
    {
        return $this->nbrAssists;
    }

    public function getNbrDeaths(): int
    {
        return $this->nbrDeaths;
    }

    /** @return array<mixed> */
    public function getItems(): array
    {
        return $this->items;
    }

    /** @return array<mixed> */
    public function getRunes(): array
    {
        return $this->runes;
    }

    /** @return array<mixed> */
    public function getSummonerSpells(): array
    {
        return $this->summonerSpells;
    }

    /**
     * @param array<mixed> $summonerSpellsParam
     */
    public function setSummonerSpells(array $summonerSpellsParam): void
    {
        $this->summonerSpells = $summonerSpellsParam;
    }

    public function getIsWin(): bool
    {
        return $this->isWin;
    }
}
