<?php

declare(strict_types=1);

namespace App\DTO;

use App\DTO\League\LeagueSoloQueueDTO;

/**
 * Class SummonerDTO : Créer un objet "SummonerDTO", pour stocker et traiter les données provenant des requêtes de l'API Riot.
 */
final class SummonerDTO
{
    private readonly string $summonerId;
    private readonly string $accountId;
    private readonly string $puuid;
    private readonly string $tagLine;
    private readonly string $gameName;
    private readonly int $profileIconId;
    private readonly int $summonerLevel;
    private ?LeagueSoloQueueDTO $leagueSoloQueue;

    public function __construct(
        string $summonerIdParam,
        string $accountIdParam,
        string $puuidParam,
        string $tagLineParam,
        string $gameNameParam,
        int $profileIconIdParam,
        int $summonerLevelParam,
        ?LeagueSoloQueueDTO $leagueSoloQueueParam,
    ) {
        $this->summonerId = $summonerIdParam;
        $this->accountId = $accountIdParam;
        $this->puuid = $puuidParam;
        $this->tagLine = $tagLineParam;
        $this->gameName = $gameNameParam;
        $this->profileIconId = $profileIconIdParam;
        $this->summonerLevel = $summonerLevelParam;
        $this->leagueSoloQueue = $leagueSoloQueueParam;
    }

    public function getSummonerId(): string
    {
        return $this->summonerId;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getPuuid(): string
    {
        return $this->puuid;
    }

    public function getTagLine(): string
    {
        return $this->tagLine;
    }

    public function getGameName(): string
    {
        return $this->gameName;
    }

    public function getProfileIconId(): int
    {
        return $this->profileIconId;
    }

    public function getSummonerLevel(): int
    {
        return $this->summonerLevel;
    }

    public function getLeagueSoloQueue(): LeagueSoloQueueDTO
    {
        return $this->leagueSoloQueue;
    }

    public function setLeagueSoloQueue(LeagueSoloQueueDTO $leagueSoloQueueDTO): void
    {
        $this->leagueSoloQueue = $leagueSoloQueueDTO;
    }
}
