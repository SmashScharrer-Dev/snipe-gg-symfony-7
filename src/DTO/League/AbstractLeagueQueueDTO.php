<?php

declare(strict_types=1);

namespace App\DTO\League;

/**
 * Class AbstractLeagueQueueDTO : Créer une classe abstraite "AbstractLeagueQueueDTO", pour pouvoir stocker dans deux sous-classes
 * les données provenant des requêtes de l'API Riot "League".
 */
abstract class AbstractLeagueQueueDTO
{
    private readonly string $leagueId;
    private readonly string $queueType;
    private readonly string $tier;
    private readonly string $rank;
    private readonly int $leaguePoints;
    private readonly int $wins;
    private readonly int $losses;

    public function __construct(
        string $leagueIdParam,
        string $queueTypeParam,
        string $tierParam,
        string $rankParam,
        int $leaguePointsParam,
        int $winsParam,
        int $lossesParam,
    ) {
        $this->leagueId = $leagueIdParam;
        $this->queueType = $queueTypeParam;
        $this->tier = $tierParam;
        $this->rank = $rankParam;
        $this->leaguePoints = $leaguePointsParam;
        $this->wins = $winsParam;
        $this->losses = $lossesParam;
    }

    public function getLeagueId(): string
    {
        return $this->leagueId;
    }

    public function getQueueType(): string
    {
        return $this->queueType;
    }

    public function getTier(): string
    {
        return $this->tier;
    }

    public function getRank(): string
    {
        return $this->rank;
    }

    public function getLeaguePoints(): int
    {
        return $this->leaguePoints;
    }

    public function getWins(): int
    {
        return $this->wins;
    }

    public function getLosses(): int
    {
        return $this->losses;
    }
}
