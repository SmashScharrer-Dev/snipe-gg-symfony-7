<?php

declare(strict_types=1);

namespace App\DTO\DDragon;

/**
 * Class DDragonSummonerSpellDTO : Créer un objet "DDragonSummonerSpellDTO", pour stocker un sort d'invocateur (ou "summoner spell"),
 * provenant des API "DDragon" et "Riot".
 */
final class DDragonSummonerSpellDTO
{
    private readonly int $id;
    private readonly string $name;
    private readonly string $description;

    public function __construct(
        int $idParam,
        string $nameParam,
        string $descriptionParam,
    ) {
        $this->id = $idParam;
        $this->name = $nameParam;
        $this->description = $descriptionParam;
    }

    public function getSummonerSpellId(): int
    {
        return $this->id;
    }

    public function getSummonerSpellName(): string
    {
        return $this->name;
    }

    public function getSummonerSpellDescription(): string
    {
        return $this->description;
    }
}
