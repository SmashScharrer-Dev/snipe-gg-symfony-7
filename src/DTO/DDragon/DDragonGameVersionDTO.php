<?php

declare(strict_types=1);

namespace App\DTO\DDragon;

/**
 * Class DDragonGameVersionDTO : Créer un objet "DragonGameVersionDTO", pour stocker la version la plus récente du jeu,
 * provenant de l'API "DDragon".
 */
final class DDragonGameVersionDTO
{
    private readonly string $latestVersion;

    public function __construct(
        string $latestVersionParam,
    ) {
        $this->latestVersion = $latestVersionParam;
    }

    public function getLatestVersion(): string
    {
        return $this->latestVersion;
    }
}
