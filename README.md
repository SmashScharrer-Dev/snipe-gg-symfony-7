<h1 align="center">Bienvenue sur le projet "Snipe-gg" 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-alpha-blue.svg?cacheSeconds=2592000" />
</p>

> Snipe-gg est une application de statistiques, utilisant des données provenant de l'API et du jeu League of Legends. Elle est développée avec le framework PHP Symfony.

### ✨ [Demo](https://snipe-demyqewiki.site/)

## Pré-requis

Un compte Riot Games développeur, sera nécessaire afin de réclamer une clé API.

Lien vers Riot Developer Portal : <https://developer.riotgames.com/apis>

## Configuration

Il sera nécessaire de configurer votre environnement de travail, et d'installer les outils, ci-dessous :

- PHP : 8.3
- Node 20.2
- NPM 9.6.6
- Composer
- Symfony CLI
- Mailpit (Optionnel)

## Installation

### Cloner le projet depuis GitLab

```sh
git clone https://gitlab.com/SmashScharrer-Dev/snipe-gg-symfony-7.git snipe-gg
cd snipe-gg
```

### Installer les dépendances PHP du projet

```sh
composer install
```

### Installer les dépendances JavaScript du projet

```sh
node install
```

### Télécharger les assets du projet (codes CSS et JavaScript)

```sh
npm run dev
```

### Ajouter votre clé API Riot Dev Portal dans un fichier .env

```sh
RIOT_API_KEY="<api_key>"
```

### Tout est bon ! C'est parti ✅

```sh
symfony server -d
```

## Auteur

👤 **GUIRADO-PATRICO Nathan**

* Website: GUIRADO-PATRICO Nathan
* Github: [@SmashScharrer-Dev](https://github.com/SmashScharrer)
* Gitlab: [@SmashScharrer-Dev](https://gitlab.com/SmashScharrer-Dev)

## 📧 Contact

[SmashScharrer-Dev@outlook.fr](mailto:SmashScharrer-Dev@outlook.fr)

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
