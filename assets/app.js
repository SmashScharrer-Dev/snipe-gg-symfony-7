import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.css';

// Import JS Packages
import 'bootstrap'; // Bootstrap 5
import tippy from 'tippy.js'; // Tooltip Library

// Libraries Styles
import 'tippy.js/dist/tippy.css'; // Tooltip Library Styles

// Global SCSS
import './styles/scss/global.scss';

export { tippy };
