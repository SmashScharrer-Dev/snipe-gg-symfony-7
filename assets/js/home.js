import Swiper from 'swiper';
import { Navigation, Autoplay } from 'swiper/modules';
import { tippy } from '../app';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/autoplay';

import '../styles/scss/home.scss';

document.addEventListener('DOMContentLoaded', function() {
  // Swiper for Summoner Cards
  const swiper = new Swiper(".mySwiper", {
    modules: [Navigation, Autoplay],
    autoplay: {
        delay: 5000,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
  });

  // Tippy tooltips for champions name
  const championsName = document.querySelectorAll('.champion-name-img');
  championsName.forEach(function(name) {
    tippy(name, {
        content: name.getAttribute('alt'),
        arrow: false
    });
  });
  
  // Select all elements with the class 'summoner-spell'
  const summonerSpells = document.querySelectorAll('.summoner-spell');

  // Apply the tooltip library to each element
  summonerSpells.forEach(function(spell) {
      tippy(spell, {
          content: spell.getAttribute('alt'),
          arrow: false
      });
  });
});
